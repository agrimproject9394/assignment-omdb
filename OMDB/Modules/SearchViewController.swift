//
//  ViewController.swift
//  OMDB
//
//  Created by DemigOD on 15/12/18.
//  Copyright © 2018 Agrim Anand. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
  
  @IBOutlet weak var bottomOFAnimatorView: NSLayoutConstraint!
  @IBOutlet weak var searchTexrField: UITextField!

  private let apiRequestForSearch = APIRequest()
  private var searchResultVc:SearchResultViewController?
  private var searchedText:String = ""
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    searchResultVc?.collectionView.collectionViewLayout.invalidateLayout()
  }

  @IBAction func searchButtonAction(_ sender: Any) {
   guard let searchText = searchTexrField.text else { return }
    searchedText = searchText
    apiRequestForSearch.pageNo = 1
    apiRequestForSearch.url = BASE_URL + "?s=\(searchText)&page=\(apiRequestForSearch.pageNo)&apikey=eeefc96f"
    APIUtils.apiCall(apiReq: apiRequestForSearch, success: { (result) in
      if let res = result {
        guard let resultDictionary = res["Search"] as? [[String:Any]] else { return }
        var searchModelList = [SearchResultModel]()
        for resultDictionaryModel in resultDictionary {
          let searchModel = SearchResultModel(fromDictionary: resultDictionaryModel)
          searchModelList.append(searchModel)
        }
        self.searchResultVc?.movieSearchResultList =  searchModelList
        self.searchResultVc?.collectionView.reloadData()
      }

    }) { (error) in
        self.showLoadingPagination(false)
        self.showAlert(heading: "Error", alertMsg: error, dismissButtonAction: {_ in
           self.searchResultVc?.isPaginating = false
        })

    }
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "searchResultCollectionView" {
      let searchResultViewController = segue.destination as? SearchResultViewController
      searchResultVc = searchResultViewController
      searchResultVc?.callbackForPagination = recieveCallbackForPagination
      searchResultVc?.showLoadingPagination = showLoadingPagination
    }
  }
  
  func recieveCallbackForPagination() {
    DispatchQueue.global(qos: DispatchQoS.QoSClass.userInitiated).async {
    self.apiRequestForSearch.url = BASE_URL + "?s=\(self.searchedText)&page=\(self.apiRequestForSearch.pageNo)&apikey=eeefc96f"
    APIUtils.apiCall(apiReq: self.apiRequestForSearch, success: { (result) in
      DispatchQueue.global(qos: DispatchQoS.QoSClass.userInteractive).async {
        if let res = result {
          guard let resultDictionary = res["Search"] as? [[String:Any]] else { return }
          var searchModelList = [SearchResultModel]()
          for resultDictionaryModel in resultDictionary {
            let searchModel = SearchResultModel(fromDictionary: resultDictionaryModel)
            searchModelList.append(searchModel)
          }
          guard let last_movies = self.searchResultVc?.movieSearchResultList else {  return }
          let latestMovies = last_movies + searchModelList
          DispatchQueue.main.async {
            self.searchResultVc?.updateCollectionView(latestMovies)
          }
        }
      }
    }) { (error) in
      
      self.showLoadingPagination(false)
      self.showAlert(heading: "Error", alertMsg: error, dismissButtonAction: {_ in
        self.searchResultVc?.isPaginating = false
      })
    }
    }
  }
  
  func showLoadingPagination(_ check:Bool) {
    DispatchQueue.main.async {
      if check {
        self.bottomOFAnimatorView.constant = 0
      } else {
        self.bottomOFAnimatorView.constant = 50
      }
      UIView.animate(withDuration: 0.6) {
        self.view.layoutIfNeeded()
      }
    }

  }
}

