//
//  SearchDetailViewController.swift
//  OMDB
//
//  Created by DemigOD on 17/12/18.
//  Copyright © 2018 Agrim Anand. All rights reserved.
//

import UIKit
import SDWebImage

class SearchDetailViewController: UIViewController {

  var searchModel:SearchResultModel?
  
  @IBOutlet weak var type: UILabel!
  @IBOutlet weak var time: UILabel!
  @IBOutlet weak var movieTitle: UILabel!
  @IBOutlet weak var movieImage: UIImageView!

  override func viewDidLoad() {
        super.viewDidLoad()
    self.navigationController?.setNavigationBarHidden(false, animated: true)
    setUpData()
  }
  
  override func viewDidDisappear(_ animated: Bool) {
    super.viewDidDisappear(animated)
    self.navigationController?.setNavigationBarHidden(true, animated: true)
  }
  
  private func setUpData() {
    type.text = searchModel?.type
    movieTitle.text = searchModel?.title
    let year = "01 Jan " + (searchModel?.year ?? "")
    if let date = year.getDateFromString(format: "dd MMM yyyy") {
      time.text = "Released " + Date().offset(from: date)
    } else {
      time.text = "From/To " + (searchModel?.year ?? "")
    }
    self.movieImage.sd_setImage(with: URL(string: searchModel?.poster ?? ""), placeholderImage:#imageLiteral(resourceName: "imagePlaceHolder"))
  }
}
