import UIKit
import SDWebImage

class SearchResultViewController: UIViewController {
  
  @IBOutlet weak var collectionView: UICollectionView!
  var movieSearchResultList: [SearchResultModel]?
  var callbackForPagination:(()->())?
  var showLoadingPagination:((Bool)->())?
  var isPaginating = false
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func updateCollectionView(_ searchModel:[SearchResultModel]) {
    showLoadingPagination?(false)
    isPaginating = false
    movieSearchResultList = searchModel
    collectionView.reloadData()
  }
  
  private func openDetailVc(_ searchModel:SearchResultModel) {
    guard let vc = storyboard?.instantiateViewController(withIdentifier: "\(SearchDetailViewController.self)") as? SearchDetailViewController else { return }
    vc.searchModel = searchModel
    self.navigationController?.pushViewController(vc, animated: true)
  }
  
}

extension SearchResultViewController: UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return movieSearchResultList?.count ?? 0
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(SearchResultCollectionViewCell.self)", for: indexPath) as? SearchResultCollectionViewCell
    cell?.title.text = movieSearchResultList?[indexPath.row].title
    cell?.type.text = movieSearchResultList?[indexPath.row].type
    let year = "01 Jan " + (movieSearchResultList?[indexPath.row].year ?? "")
    if let date = year.getDateFromString(format: "dd MMM yyyy") {
      cell?.time.text = Date().offset(from: date)
    } else {
       cell?.time.text = movieSearchResultList?[indexPath.row].year
    }
    cell?.movieImage.sd_setImage(with: URL(string: movieSearchResultList?[indexPath.row].poster ?? ""), placeholderImage:#imageLiteral(resourceName: "imagePlaceHolder"))
    return cell!
  }
  
  func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    
    if indexPath.row  == (movieSearchResultList?.count ?? 0) - 3 &&  isPaginating == false {
      showLoadingPagination?(true)
      self.isPaginating = true
      self.callbackForPagination?()
    }
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let searchModel = movieSearchResultList?[indexPath.row] else { return }
    openDetailVc(searchModel)
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let font =  UIFont.init(name: "Rockwell-Bold", size: 15.0)
    let title = movieSearchResultList?[indexPath.row].title
    let height = title?.height(withConstrainedWidth:  collectionView.frame.size.width - 130, font: font!)
    return CGSize(width: ((collectionView.frame.size.width-30)/2), height: 100 + (((height ?? 0)*3)))
  }
  
}
