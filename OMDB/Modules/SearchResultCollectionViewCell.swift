//
//  SearchResultCollectionViewCell.swift
//  OMDB
//
//  Created by DemigOD on 16/12/18.
//  Copyright © 2018 Agrim Anand. All rights reserved.
//

import UIKit

class SearchResultCollectionViewCell: UICollectionViewCell {
    
  @IBOutlet weak var movieImage: UIImageView!
  @IBOutlet weak var type: UILabel!
  @IBOutlet weak var title: UILabel!
  @IBOutlet weak var time: UILabel!
  
}
