//
//  StringExtension.swift
//  OMDB
//
//  Created by DemigOD on 16/12/18.
//  Copyright © 2018 Agrim Anand. All rights reserved.
//

import Foundation
import UIKit


extension String {
  func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
    
    return ceil(boundingBox.height)
  }
  
  func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
    let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
    let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
    
    return ceil(boundingBox.width)
  }
  
  func getDateFromString(format:String) -> Date? {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format
    guard let date = dateFormatter.date(from: self) else {
      return nil
    }
    return date
  }
}

extension UIViewController {
  
  func showAlert(heading:String,alertMsg:String ,dismissButtonAction:((UIAlertAction)->())?) {
    DispatchQueue.main.async {
      let controller = UIAlertController(title: heading, message: alertMsg, preferredStyle: UIAlertController.Style.alert)
      let alertAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.cancel, handler: dismissButtonAction)
      controller.addAction(alertAction)
      self.present(controller, animated: true, completion: nil)
    }
  }
  
}


extension Date {
  /// Returns the amount of years from another date
  func years(from date: Date) -> Int {
    return Calendar.current.dateComponents([.year], from: date, to: self).year ?? 0
  }
  /// Returns the amount of months from another date
  func months(from date: Date) -> Int {
    return Calendar.current.dateComponents([.month], from: date, to: self).month ?? 0
  }
  /// Returns the amount of weeks from another date
  func weeks(from date: Date) -> Int {
    return Calendar.current.dateComponents([.weekOfMonth], from: date, to: self).weekOfMonth ?? 0
  }
  /// Returns the amount of days from another date
  func days(from date: Date) -> Int {
    return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
  }
  /// Returns the amount of hours from another date
  func hours(from date: Date) -> Int {
    return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
  }
  /// Returns the amount of minutes from another date
  func minutes(from date: Date) -> Int {
    return Calendar.current.dateComponents([.minute], from: date, to: self).minute ?? 0
  }
  /// Returns the amount of seconds from another date
  func seconds(from date: Date) -> Int {
    return Calendar.current.dateComponents([.second], from: date, to: self).second ?? 0
  }
  /// Returns the amount of nanoseconds from another date
  func nanoseconds(from date: Date) -> Int {
    return Calendar.current.dateComponents([.nanosecond], from: date, to: self).nanosecond ?? 0
  }
  /// Returns the a custom time interval description from another date
  func offset(from date: Date) -> String {
    if years(from: date)   > 0 { return "\(years(from: date))" + posttextForDate   }
    if months(from: date)  > 0 { return "\(months(from: date)) Month Ago"  }
    if weeks(from: date)   > 0 { return "\(weeks(from: date)) Week Ago"   }
    if days(from: date)    > 0 { return "\(days(from: date)) Day Ago"    }
    if hours(from: date)   > 0 { return "\(hours(from: date)) Hours Ago"   }
    if minutes(from: date) > 0 { return "\(minutes(from: date)) Minutes Ago"  }
    if seconds(from: date) > 0 { return "\(seconds(from: date)) Seconds Ago"  }
    if nanoseconds(from: date) > 0 { return "\(nanoseconds(from: date))ns"  }
    return ""
  }
  
  func getStringDate(_ format:String?) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = format ?? "dd-mm-yyyy" //Your date format
    //    dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
    //according to date format your date string
    return dateFormatter.string(from: self)
    
  }
  
  
}

let posttextForDate = " Years Ago"
