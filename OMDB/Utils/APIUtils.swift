//
//  APIUtils.swift
//  OMDB
//
//  Created by DemigOD on 16/12/18.
//  Copyright © 2018 Agrim Anand. All rights reserved.
//

import UIKit

class APIUtils: NSObject {

  class func apiCall(apiReq:APIRequest,success:(([String:Any]?)->Void)?,failure:((String)->Void)?) {
      guard let url = URL(string: apiReq.url) else { return }
      var request = URLRequest(url: url)
      request.httpMethod = apiReq.method?.rawValue
      request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
      let session = URLSession.shared
      session.dataTask(with: request) { (data, response, error) in
        if let data = data {
          do {
            let parsedObject = try JSONSerialization.jsonObject(with: data, options:[])
            let parse = parsedObject as! [String:Any]
            if ((parse["Response"] as! String) == "False") {
              if let errorMsg = (parse["Error"] as? String) {
                failure?(errorMsg)
                return
              }
            }
            DispatchQueue.main.async {
              success?(parsedObject as? [String:Any])
              apiReq.pageNo += 1
            }
          }catch let error {
            print(error)
            DispatchQueue.main.async {
              failure?(error.localizedDescription)
            }
          }
        }
        }.resume()
    }
}


class APIRequest {
  var url:String = ""
  var params:[String:Any]?
  var authentication:[String:String]?
  var method:APIType?
  var pageNo = 0
}

enum APIType:String {
  case get = "GET"
  case post = "POST"
}

let BASE_URL = "https://www.omdbapi.com"
